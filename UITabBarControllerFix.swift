//
//  UITabBarControllerFix.swift
//  BeepIn
//
//  Created by Carlos Orrego on 11/18/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import Foundation

// Soluciona un problema con autolayout presente en iOS 7. Mas info en 
// http://stackoverflow.com/questions/22812211/ios-7-views-change-sizes-when-using-tabbar-and-autolayout
// El problema es que los elementos anclados al limite inferior se mueven luego de seleccionar
// otro tab.
class UITabBarControllerFix:UITabBarController, UITabBarControllerDelegate{
    override func viewDidLoad() {
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN FIX.INIT')")
        super.viewDidLoad()
        self.delegate = self
        //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN FIX.INIT')")
    }
    
    func tabBarController(tabBarController: UITabBarController,
        didSelectViewController viewController: UIViewController){
            //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN FIX.TABBARCONTROLLER')")
            tabBarController.view.setNeedsLayout()
            //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN FIX.TABBARCONTROLLER')")
    }
}