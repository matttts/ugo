//
//  AppDelegate.swift
//  ReportApp
//
//  Created by Carlos Orrego on 8/8/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit
import Foundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?
    var user: String = ""
    var beepIn = BeepIn.sharedInstance

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary?) -> Bool {
        // Override point for customization after application launch.
        /*Keychain.save("corrego", data: "test".dataUsingEncoding(NSUTF8StringEncoding)!)
        let test = Keychain.load("corrego")*/
        
        // background fetch genera este evento tambien, si es asi, nos lo saltamos
        if(application.applicationState == .Background){
            return true
        }
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)

        SD.executeChange("CREATE TABLE IF NOT EXISTS offline ( pkey INTEGER PRIMARY KEY AUTOINCREMENT, fch_dispositivo TEXT, nro_retraso_sec TEXT, val_latitud TEXT, val_longitud TEXT, val_altura TEXT, val_precision TEXT, tipo_evento TEXT, tipo_dispositivo TEXT, es_mock_location TEXT, es_registro_offline TEXT, beacon_primario_major TEXT, beacon_primario_minor TEXT, beacon_secundario_major TEXT, beacon_secundario_minor TEXT, user TEXT)")
        
        //SD.executeChange("CREATE TABLE IF NOT EXISTS crashlog (flag TEXT)")
        SD.executeChange("CREATE TABLE IF NOT EXISTS latabla (lacolumna TEXT)")
        
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        return true
    }
    
    func application(application: UIApplication!, performFetchWithCompletionHandler completionHandler: ((UIBackgroundFetchResult) -> Void)!) {
        NSLog("en fetch")
        beepIn.enviarUnoOffline({result in
            switch (result){
            case .Success:
                NSLog("mandamos 1")
            completionHandler(UIBackgroundFetchResult.NewData)
            default:
                NSLog("no se pudo")
            completionHandler(UIBackgroundFetchResult.Failed)
            }
        })
    }

    func applicationWillResignActive(application: UIApplication!) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication!) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication!) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication!) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication!) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

