//
//  BackgroundUIImageView.swift
//  BeepIn
//
//  Created by Carlos Orrego on 11/10/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import Foundation

class BackgroundUIImageView: UIImageView{
    init(view: UIView!, image:UIImage!){
        super.init(image: image)
        
        self.contentMode = .ScaleAspectFit
        self.clipsToBounds = true
        // modificar frame para tomar la pantalla completa
        // por defecto el frame de la vista no incluye la barra de arriba. La agregamos
        var frame = computeRect(self.frame, viewRect: view.frame)
        self.frame = frame
        self.tag = 1234
        
        // agregar a la vista
        view.addSubview(self)
        //view.sendSubviewToBack(lightBlurView)
        view.sendSubviewToBack(self)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // calcula las dimensiones de la imagen para conformar a la pantalla
    private func computeRect(imageRect: CGRect, viewRect: CGRect) -> CGRect {
        var rect = CGRect()
        var aspect = imageRect.width / imageRect.height
        
        if  (viewRect.width / aspect > viewRect.height){
            rect.size.width = viewRect.width
            rect.size.height = viewRect.width/aspect
        } else{
            rect.size.height = viewRect.height
            rect.size.width = viewRect.height*aspect
        }
        
        return rect
    }
}