//
//  internetManager.swift
//  BeepIn
//
//  Created by Matías Mella on 11/27/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit

class internetManager: NSObject {
    
    var reachability:Reachability?
   
    func hasConnectivity() -> Int {
        let reachability: Reachability = Reachability.reachabilityForInternetConnection()
        let networkStatus: Int = reachability.currentReachabilityStatus().value
        
        if networkStatus == 0 {
            return 0
        }
        else {
            return 1
        }
    }
}
