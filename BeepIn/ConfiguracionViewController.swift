//
//  ConfiguracionViewController.swift
//  BeepIn
//
//  Created by Matías Mella on 11/26/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit

class ConfiguracionViewController: UIViewController {

    @IBOutlet weak var usuario: UILabel!
    @IBOutlet weak var btnCerrarSesion: UIButton!
    @IBOutlet weak var btnEnviarRegistros: UIButton!
    @IBOutlet weak var lblRegistrosOffline: UILabel!
    @IBOutlet weak var lblUltimoEnvio: UILabel!
    
    let persistedSettings = PersistedSettings.sharedInstance
    let beepIn = BeepIn.sharedInstance
    var cantOffline: Int?
    let internetman = internetManager()
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        let subViews: Array = self.view.subviews
        for sub in subViews
        {
            if sub.tag == 1234 {
                sub.removeFromSuperview()
                var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBarHidden = true
        
        var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
    }
    
    override func viewWillAppear(animated: Bool) {
        if internetman.hasConnectivity() != 1 {
            btnCerrarSesion.hidden = true
        }
        else {
            btnCerrarSesion.hidden = false
        }
        
        usuario.text = "Bienvenido, "+persistedSettings.user!
        
        let (resultSet, err) = SD.executeQuery("SELECT * FROM offline")
        if err == nil {
            self.cantOffline = resultSet.count
            
            if resultSet.count == 0 {
                self.lblRegistrosOffline.text = "No hay registros Offline"
            }
            else {
                self.lblRegistrosOffline.text = "Hay \(self.cantOffline!) registros Offline por enviar"
            }
        }
    
        getDatosOffline()
    }
    
    func getDatosOffline() {
        let format = "dd-MM-yyyy '-' HH:mm"
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        
        if let offline = persistedSettings.lastOffline {
            println("entre")
            lblUltimoEnvio.text = "Último envío: " + formatter.stringFromDate(offline)
        } else {
            lblUltimoEnvio.text = "No existen envíos"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHUD(string: String? = nil){
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        if let text = string {
            hud.labelText = text
        }
    }
    
    func hideHUD() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    //[dlopez]
    // se actualiza la func para que alerte al usuario que se eliminarán los registros offline
    @IBAction func btnCerrarSesionTouchUp(sender: AnyObject) {
        if cantOffline! != 0 {
            var alert = UIAlertController(title: "Existen registros sin enviar", message: "Si continúa los registros se borrarán permanentemente.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Continuar", style: .Default, handler: {action in
                self.cerrarSesion()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            cerrarSesion()
        }
    }
    
    //[dlopez]
    // este código era el antiguo btnCerrarSessionTouchUp
    
    func cerrarSesion() {
        showHUD(string: "Cerrando sesión...")
        persistedSettings.user = nil
        beepIn.logOut()
        hideHUD()
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func btnEnviarRegistrosTouchUp(sender: AnyObject) {
        if cantOffline! == 0 {
            var alert: UIAlertView = UIAlertView()
            alert.title = "No hay registros"
            alert.message = "Usuario no tiene registros Offline para enviar."
            alert.addButtonWithTitle("Aceptar")
            alert.show()
        }
        else {
            if internetman.hasConnectivity() == 1 {
                showHUD(string: "Enviando...")
                beepIn.enviarOffline()
                getDatosOffline()
                self.lblRegistrosOffline.text = "No hay registros Offline"
                self.cantOffline = 0
                hideHUD()
            }
            else {
                var alert: UIAlertView = UIAlertView()
                alert.title = "No hay conexión a internet"
                alert.message = "Compruebe su conexión a internet e intente nuevamente."
                alert.addButtonWithTitle("Aceptar")
                alert.show()
            }
        }
    }

}
