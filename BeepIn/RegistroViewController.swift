//
//  ViewController.swift
//  ReportApp
//
//  Created by Carlos Orrego on 8/8/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

let uuid = NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")

class RegistroViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var btnIngreso: UIButton!
    @IBOutlet weak var btnSalida: UIButton!
    @IBOutlet weak var lblUltimoIngreso: UILabel!
    @IBOutlet weak var lblUltimaSalida: UILabel!
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var vwUltimas: UIView!
    @IBOutlet weak var imgBeaconStatus: UIImageView!
    @IBOutlet weak var imgOnlineStatus: UIImageView!
    @IBOutlet weak var imgLocationStatus: UIImageView!

    var beepIn = BeepIn.sharedInstance
    var reportType = ReportType.Arrival
    var locationManager: CLLocationManager!
    var persistedSettings = PersistedSettings.sharedInstance
    var reachability:Reachability?
    var tengointernet: Int = 0
    var semaforoL = 1
    let region = CLBeaconRegion(proximityUUID: uuid, identifier: "Estimote Region")
    var nombrebeaconcercano: String!
    var potenciabeaconcercano: Int!
    var lastLocation: CLLocation?
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        let subViews: Array = self.view.subviews
        for sub in subViews
        {
            if sub.tag == 1234 {
                sub.removeFromSuperview()
                var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN REGISTRO.VIEWDIDLOAD')")
        
        //var lado: CGFloat = UIScreen.mainScreen().bounds.height * 0.29
            var lado: CGFloat = UIScreen.mainScreen().bounds.height * 0.25
    
//            var image: UIImage = UIImage(named:"btnMarcarIngreso.fw.png")!
//            var newSize: CGSize = CGSize(width: lado, height: lado)
//            let rect = CGRectMake(0, 0, lado, lado)
//            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
//            image.drawInRect(rect)
//            let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        
//            UIGraphicsEndImageContext()
//            btnIngreso.setBackgroundImage(newImage, forState: .Normal)
//                
//            image = UIImage(named:"btnMarcarSalida.fw.png")!
//            newSize = CGSize(width: lado, height: lado)
//            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
//            image.drawInRect(rect)
//            let newImage2 = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            btnSalida.setBackgroundImage(newImage2, forState: .Normal)
        
        var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
        
        if persistedSettings.user! == "mmella" || persistedSettings.user! == "corrego" {
            if let info = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String {
                if let info2 = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String {
                    version.text = "Versión: \(info), Build: \(info2)"
                }
            }
        }
        else {
            version.hidden = true
        }
        
        initLocationManager()
        loadLastReports()
        initReachabilityManager()
        
        showHUD(string: "Iniciando...")
        
        // boton redondo
        /*btnIngreso.titleLabel!.textAlignment = .Center
        btnIngreso.layer.cornerRadius = btnIngreso.bounds.height/2
        btnIngreso.layer.masksToBounds = true
        
        btnSalida.titleLabel!.textAlignment = .Center
        btnSalida.layer.cornerRadius = btnSalida.bounds.height/2
        btnSalida.layer.masksToBounds = true*/
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "appWentToBackground:",
            name: UIApplicationDidEnterBackgroundNotification,
            object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "appCameToForeground:",
            name: UIApplicationWillEnterForegroundNotification,
            object: nil)
        
        enviarGuardados() //envio offline al iniciar aplicacion
    }
    
    func appWentToBackground(notification: NSNotification) {
        println("BACKGROUND")
        stopLocation()
    }
    
    func appCameToForeground(notification: NSNotification) {
        println("FOREGROUND")
        startLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        println("sali")
        stopLocation()
    }
    
    override func viewDidAppear(animated: Bool) {
        println("volvi")
        startLocation()
        if(hasConnectivity() == 1) {
            tengointernet = 1
            self.litReachable(true)
        }
        else {
            tengointernet = 0
            self.litReachable(false)
        }
    }
    
    func showHUD(string: String? = nil){
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        if let text = string {
            hud.labelText = text
        }
    }
    
    func initReachabilityManager() {
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN REGISTRO.RECHABILITYMANAGER')")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reachabilityChanged", name:kReachabilityChangedNotification, object: nil)
        
        reachability = Reachability(hostName: "www.google.com");
        
        reachability?.startNotifier();
        //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN REGISTRO.RECHABILITYMANAGER')")
    }
    
    func enviarGuardados() {
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN REGISTRO.ENVIARGUARDADOS')")
        if(hasConnectivity() == 1) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.beepIn.enviarOffline()
            })
        }
        //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN REGISTRO.ENVIARGUARDADOS')")
    }
    
    func litReachable(reachable: Bool)
    {
        if reachable
        {
            imgOnlineStatus.alpha = 1
        }
        else
        {
            imgOnlineStatus.alpha = 0.3
        }
    }
    
    func litBeacon(beacon: Bool)
    {
        if beacon
        {
            imgBeaconStatus.alpha = 1
        }
        else
        {
            imgBeaconStatus.alpha = 0.3
        }
    }
    
    func litLocation(location: Bool)
    {
        if location
        {
            imgLocationStatus.alpha = 1
        }
        else
        {
            imgLocationStatus.alpha = 0.3
        }
    }
    
    func reachabilityChanged()
    {
        if tengointernet == 0 {
            litReachable(false)
        }
        
        if(hasConnectivity() == 1) {
                if(hasConnectivity() == 1) {
                    //self.enviarGuardados()
                    if (self.tengointernet == 0) {
                        self.beepIn.testConnectionAndCredentials{result in
                            switch result {
                                case .NoConnection:
                                    self.hideHUD()
                                    self.litReachable(false)
                                    //self.btnCerrarSesion.hidden = true
                                case .ServerError:
                                    self.hideHUD()
                                    self.litReachable(false)
                                    //self.btnCerrarSesion.hidden = true
                                case .NoCredentials:
                                    self.hideHUD()
                                    self.tengointernet = 1
                                    self.litReachable(true)
                                    //self.btnCerrarSesion.hidden = false
                                case .Success:
                                    self.tengointernet = 1
                                    self.litReachable(true)
                                    //self.updateUser()
                                    self.hideHUD()
                                    //self.btnCerrarSesion.hidden = false
                                default:
                                    break
                            }
                        }
                    }
                }
                else {
                    self.hideHUD()
                    tengointernet = 0
                    self.litReachable(false)
                }
        }
        else {
            //self.updateUser()
            self.hideHUD()
            tengointernet = 0
            self.litReachable(false)
            //self.btnCerrarSesion.hidden = true
        }
    }
    
    func hideHUD() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    func showLogin(){
        var alert = UIAlertView(title: "Inicio de Sesión", message: "Ingrese nombre de usuario y contraseña", delegate: self, cancelButtonTitle: "Ingresar")
        alert.alertViewStyle = .LoginAndPasswordInput
        alert.show()
        hideHUD()
    }
    
    func hasConnectivity() -> Int {
        let reachability: Reachability = Reachability.reachabilityForInternetConnection()
        let networkStatus: Int = reachability.currentReachabilityStatus().value

        if networkStatus == 0 {
            return 0
        }
        else {
            return 1
        }
    }
    
    @IBAction func btnEntradaTouchUp(sender: AnyObject) {
        showHUD(string: "Guardando...")
        reportType = .Arrival
        locationManager.startUpdatingLocation()
        enviar()
    }
    
    @IBAction func btnSalidaTouchUp(sender: AnyObject) {
        showHUD(string: "Guardando...")
        reportType = .Departure
        locationManager.startUpdatingLocation()
        enviar()
    }
    
//    @IBAction func btnCerrarSesionTouchUp(sender: AnyObject) {
//        showHUD(string: "Cerrando sesión...")
//        //lblUser.text = "Bienvenido"
//        persistedSettings.user = nil
//        beepIn.logOut()
//        self.navigationController?.popToRootViewControllerAnimated(true)
//        stopLocation()
//        hideHUD()
//    }
    
    func enviar() {
        if let location = lastLocation {
            reportWithLocation(location: location)
        }
        else {
            reportWithLocation()
        }
    }
    
    func reportWithLocation(location: CLLocation? = nil){
        if semaforoL == 1 {
            
        semaforoL = 0
        switch reportType{
        case .Arrival:
            beepIn.reportArrival(location, completed: {success in
                self.semaforoL = 1
                self.persistedSettings.lastArrival = NSDate()
                self.reportSaved(success)
            })
        case .Departure:
            beepIn.reportDeparture(location, completed: {success in
                self.semaforoL = 1
                self.persistedSettings.lastDeparture = NSDate()
                self.reportSaved(success)
            })
        }

        }
    }
    
    func reportSaved(success: Bool){
        hideHUD()
        if success{
            NSLog("guardado")
        } else {
            NSLog("error al guardar")
        }
        
        loadLastReports()
    }
    
    func loadLastReports(){
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN REGISTRO.LOADLASTREPORTS')")
        let format = "dd-MM-yyyy '-' HH:mm"
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        
        if let arrive = persistedSettings.lastArrival {
            lblUltimoIngreso.text = "Último ingreso: " + formatter.stringFromDate(arrive)
        } else {
            lblUltimoIngreso.text = "No se registran ingresos"
        }
        
        if let leave = persistedSettings.lastDeparture {
            lblUltimaSalida.text = "Última salida: " + formatter.stringFromDate(leave)
        } else {
            lblUltimaSalida.text = "No se registran salidas"
        }
        //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN REGISTRO.LOADLASTREPORTS')")
    }
    
//    func updateUser(){
//        if let user = persistedSettings.user {
//            lblUser.text = "Bienvenido, \(user)"
//        } else {
//            lblUser.text = "Bienvenido"
//        }
//    }
    
    
}

extension RegistroViewController : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("ERROR: \(error)")
        lastLocation = nil
        litLocation(false)
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as CLLocation

        lastLocation = locationObj
        litLocation(true)
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.NotDetermined) {
            println("Auth status unkown still!");
        }
        
        locationManager.startUpdatingLocation()
    }
    
    // beacons
    func locationManager(manager: CLLocationManager!, didStartMonitoringForRegion region: CLRegion!) {
        NSLog("Did start monitoring for region")
    }
    
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        NSLog("Did enter region!")
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {

        if beacons.count != 0 {
            litBeacon(true)
        }
        else {
            litBeacon(false)
        }
        
//        if let beaconsArray = beacons as? [CLBeacon] {
//            var beacons = Dictionary<String, Int>()
//            for var i = 0; i < beaconsArray.count; ++i {
//                let beacon_i = beaconsArray[i]
//                
//                var nombre = "\(beacon_i.major),\(beacon_i.minor)"
//                
//                beacons["\(nombre)"] = beacon_i.rssi
//            }
//            var menor = 0
//            var beaconmenor = ""
//            for (nombrebeacon, rssi) in beacons {
//                //println("\(nombrebeacon) \(rssi)")
//                if menor == 0 {
//                    menor = rssi
//                }
//                if menor < rssi && rssi != 0{
//                    menor = rssi
//                    beaconmenor = nombrebeacon
//                    //beacon.text? = beaconmenor
//                }
//            }
//            nombrebeaconcercano = beaconmenor
//            potenciabeaconcercano = menor
//            //beacon.text? = beaconmenor
//        }
//        //beacon.text? = nombrebeaconcercano
//        //println(nombrebeaconcercano)
//        //println(potenciabeaconcercano)
    }
    
    func initLocationManager() {
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN REGISTRO.INITLOCATIONMANAGER')")
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // api de ios8
        //locationManager.requestWhenInUseAuthorization()
        var status = CLLocationManager.authorizationStatus()
        //self.showLogin()
        // api de ios8
        /*if (locationManager.respondsToSelector(Selector("requestWhenInUseAuthorization"))) {
            locationManager.requestWhenInUseAuthorization()
        }*/
        
        if (locationManager.respondsToSelector(Selector("requestAlwaysAuthorization"))) {
            locationManager.requestAlwaysAuthorization()
        }
        
        //startLocation()
        //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN REGISTRO.INITLOCATIONMANAGER')")
    }
    
    func startLocation() {
        println("STARTLOCATION")
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN REGISTRO.STARTLOCATIONMANAGER')")
        locationManager.startMonitoringForRegion(region)
        locationManager.startRangingBeaconsInRegion(region)
        locationManager.startUpdatingLocation()
        //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN REGISTRO.STARTLOCATIONMANAGER')")
    }
    
    func stopLocation() {
        println("STOPLOCATION")
        locationManager.stopMonitoringForRegion(region)
        locationManager.stopRangingBeaconsInRegion(region)
        locationManager.stopUpdatingLocation()
    }
}

enum ReportType {
    case Arrival
    case Departure
}
