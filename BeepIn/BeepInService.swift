//
//  ServiceWrapper.swift
//  ReportApp
//
//  Created by Carlos Orrego on 8/21/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import Foundation

class BeepInService : NSObject
{
    private let baseUrl = "http://apps.solu4b.com/beepinservice/BeepInDataService.svc"
    private let realm = "SOLUCIONES"
    
    //let protectionSpace : NSURLProtectionSpace
    
    //let request:NSURLRequest! // "implicitly unwrapped optional"
    //let session:NSURLSession!
    //let settings = PersistedSettings()
    var credential : NSURLCredential
    //var failureCount = 0
    var semaforoD = 1 //1 verde, 0 rojo
    //var tengoCredencial : Bool = false
    
    /*override init()
    {
        
        //let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        //session = NSURLSession(configuration: config)
        //protectionSpace = NSURLProtectionSpace(host: "apps.solu4b.com", port: 80, `protocol`: "http", realm: self.realm, authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
        
        // recuperar credenciales
        if let user = settings.user {
            tengoCredencial = true
            //let password = Keychain.load(user)
            let (resultSet, err) = SD.executeQuery("SELECT * FROM keychain")
            if err != nil || resultSet.count == 0 {
                //there was an error during the query, handle it here
                credential = NSURLCredential()
            } else {
                var row = resultSet.first!
                let password = row["result"]!.asString()!
                credential = NSURLCredential(user: user, password: password, persistence: .None)
            }
        } else {
            tengoCredencial = false
            credential = NSURLCredential()
        }
        super.init()
    }*/
    
    init(account: Account? = nil) {
        if let act = account {
            NSLog("con cred")
            credential = NSURLCredential(user: act.userName, password: act.secret!, persistence: .None)
            //tengoCredencial = true
        }
        else {
            NSLog("sin cred")
            credential = NSURLCredential(user: "", password: "", persistence: .None)
            //tengoCredencial = false
        }
    }
    
    func testConnection(completed:(ConnectionResult -> Void))
    {
        NSLog(credential.description)
        if /*tengoCredencial == false || settings.user == nil*/ (credential.user!.isEmpty) {
            completed(.NoCredentials)
        }
            
        else {
            Alamofire.manager.request(.GET, baseUrl)
                .authenticate(usingCredential: credential)
                .responseString{(request, response, string, error) in
                    
                    if let hasError = error {
                        switch error!.code{
                        case -999:
                            completed(.NoCredentials)
                        default:
                            completed(.ServerError)
                        }
                        //completed(.NoConnection)
                    } else {
                        switch response!.statusCode {
                        case 200:
                            //self.tengoCredencial = true
                            completed(.Success)
                        case 401:
                            completed(.NoCredentials)
                        case 500:
                            completed(.ServerError)
                        default:
                            completed(.ServerError)
                        }
                    }
            }
        }
    }
    
    func testConnectionWithCredential(credential: NSURLCredential!, completed:(ConnectionResult -> Void))
    {
        if credential.user == "" || credential.password == "" {
            completed(.NoCredentials)
            return
        }
        
        Alamofire.manager.request(.GET, baseUrl)
            .authenticate(usingCredential: credential)
            .responseString{(request, response, string, error) in
                
                if let hasError = error {
                    switch error!.code{
                    case -999:
                        completed(.NoCredentials)
                    default:
                        completed(.ServerError)
                    }
                }
                else {
                    switch response!.statusCode {
                    case 200:
                        //self.tengoCredencial = true
                        self.credential = credential
                        completed(.Success)
                    case 401:
                        completed(.NoCredentials)
                    case 500:
                        completed(.ServerError)
                    default:
                        completed(.ServerError)
                    }
                }
        }
    }
    
    func estadoUsuario(completed: (AccionPermitida -> Void)) {
        Alamofire.manager.request(.GET, baseUrl+"/ConsultarEstadoUsuario")
            .authenticate(usingCredential: credential)
            .responseString{(request, response, string, error) in
                
                if let hasError = error {
                    println("Error en estado usuario")
                } else {
                    var primer = string!.componentsSeparatedByString(">")
                    var segundo = primer[2].componentsSeparatedByString("<")
                    if segundo[0] == "I" {
                        completed(.Ingreso)
                    }
                    else if segundo[0] == "S" {
                        completed(.Salida)
                    }
                    else {
                        completed(.nada)
                    }
                }
        }
    }
    
    func endSession() {
        //protectionSpace.finalize()
        //session.finishTasksAndInvalidate()
        //Alamofire.manager.session.finishTasksAndInvalidate()
        credential = NSURLCredential()
        //tengoCredencial = false
        //settings.user = nil
    }
    
    func obtieneRegistros(completed: (([[String:String]], Bool) -> Void)) {
        var registros: [[String:String]] = []
        
        // todo: en ios 7 no carga los nuevos registros
        Alamofire.manager.request(.GET, baseUrl+"/LeerAsistenciaUltimosDias?$format=json")
            .authenticate(usingCredential: credential)
            .responseJSON { (_, response, JSON, error) in
                println(error)
                println(response)
                println(JSON)
                if let hasError = error {
                    println("ERROR EN REGISTROS")
                    completed(registros, false)
                } else {
                    // ios 7 no maneja bien los errores 500 con body!!
                    // obtenemos el diccionario y verificamos que traiga la "d"
                    var json = JSON as NSDictionary
                    
                    if (json.objectForKey("d") != nil) {
                        var datos = json["d"] as NSDictionary
                        let informacion = datos["results"]! as [[String : AnyObject]]
                        var temp = [String:String]()
                        for info in informacion {
                            temp["tipo_dispositivo"] = info["tipo_dispositivo"] as? String
                            
                            if info["tipo_evento"] as? String == "S" {
                                temp["tipo_evento"] = "Salida"
                            }
                            else {
                                temp["tipo_evento"] = "Entrada"
                            }
                            
                            var date = (info["es_registro_offline"] as? Bool == true ? info["fch_dispositivo"]:info["fch_servidor"]) as String
                            
                            var stamp = date.componentsSeparatedByString("T")
                            var hhmmss = stamp[1].componentsSeparatedByString(".")
                            var hora = hhmmss[0].componentsSeparatedByString(":")
                            var fecha = stamp[0].componentsSeparatedByString("-")
                            temp["fch_servidor"] = "\(fecha[2])/\(fecha[1])/\(fecha[0]) - \(hora[0]):\(hora[1])"
                            temp["val_latitud"] = info["val_latitud"] as? String
                            temp["val_longitud"] = info["val_longitud"] as? String
                            temp["dispositivo"] = info["tipo_dispositivo"] as? String
                            var beep_id: Int! = info["BEEP_ID"] as Int
                            let xNSNumber = beep_id as NSNumber
                            temp["beep_id"] = xNSNumber.stringValue
                            registros.append(temp)
                        }
                    }
                    
                    // obtener 6 o los que haya.... TODOS LOS REGISTROS
                    //if registros.count > 6 {
                    //    var registros_finales = Array(registros[0...6])
                    //    completed(registros_finales, true)
                    //}
                    //else {
                        completed(registros, true)
                    //}
                }
        }
    }
    
    func reportArrival(parameters: [String : AnyObject]!, completed: (ConnectionResult -> Void)) {
        
        Alamofire.manager.request(.GET, baseUrl+"/RegistrarAsistencia", parameters: parameters)
            .authenticate(usingCredential: credential)
            .responseString{(request, response, string, error) in
                
                if let hasError = error {
                    self.semaforoD = 1
                    completed(.NoConnection)
                }
                else {
                    self.semaforoD = 1
                    switch response!.statusCode {
                    case 200:
                        completed(.Success)
                    case 401:
                        completed(.NoCredentials)
                    case 500:
                        completed(.ServerError)
                    default:
                        completed(.ServerError)
                    }
                }
        }
        
    }
    
    func reportDeparture(parameters: [String : AnyObject]!, completed: (ConnectionResult -> Void)) {
        // mock, cambiar despues si cambia el endpoint
        println(parameters)
        reportArrival(parameters, completed)
    }
    
    func annulRegister(parameters: [String : Int]!, completed: (ConnectionResult -> Void)) {
        
        Alamofire.manager.request(.GET, baseUrl+"/AnularRegistro", parameters: parameters)
            .authenticate(usingCredential: credential)
            .responseString{(request, response, string, error) in
                
                if let hasError = error {
                    self.semaforoD = 1
                    completed(.NoConnection)
                }
                else {
                    self.semaforoD = 1
                    switch response!.statusCode {
                    case 200:
                        completed(.Success)
                    case 401:
                        completed(.NoCredentials)
                    case 500:
                        completed(.ServerError)
                    default:
                        completed(.ServerError)
                    }
                }
        }
        
    }
}

enum ConnectionResult{
    case Success
    case NoCredentials
    case NoConnection
    case ServerError
}

enum AccionPermitida{
    case Ingreso
    case Salida
    case nada
}