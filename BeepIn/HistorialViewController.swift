//
//  ultimosRegistrosViewController.swift
//  BeepIn
//
//  Created by Carlos Orrego on 11/6/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit
import Foundation
import MapKit

class HistorialViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var fondoMapa: UILabel!
    @IBOutlet weak var cerrarMapa: UIButton!

    
    var mapHidden: Bool!
    var registros: NSDictionary?
    var items: [[String:String]] = []
    let beepIn = BeepIn.sharedInstance
    var refresher: UIRefreshControl!
    var xCentroMapa: CGFloat = 0
    var yCentroMapa: CGFloat = 0
    var regionInicial: MKCoordinateRegion!
    
    var ladomapa: CGFloat!
    var centromapa: CGFloat!
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        let subViews: Array = self.view.subviews
        for sub in subViews
        {
            if sub.tag == 1234 {
                sub.removeFromSuperview()
                var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        mapHidden = true
        mapView.hidden = true
        fondoMapa.hidden = true
        xCentroMapa = UIScreen.mainScreen().bounds.width/2
        yCentroMapa = UIScreen.mainScreen().bounds.height/2
//        self.centromapa = self.mapView.frame.width/2
//        self.ladomapa = self.mapView.frame.width
//        
//        println(self.mapView.frame.width)
//        println(self.mapView.frame.height)
        
        mapView.frame = CGRect(x:xCentroMapa, y:yCentroMapa, width: 0, height: 0)
    }
    
    override func viewDidLayoutSubviews() {
        self.centromapa = self.mapView.frame.width/2
        self.ladomapa = self.mapView.frame.width
        
        println(self.mapView.frame.width)
        println(self.mapView.frame.height)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBarHidden = true
        self.tabla.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tabla.separatorStyle = UITableViewCellSeparatorStyle.None
        
        var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
        
        // unica forma de hacer el table view transparente
        self.tabla.backgroundView = nil
        self.tabla.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.0)
        self.tabla.rowHeight = 48
        
        // [DLOPEZ]
        // pullToRefresh para actualizar la lista de registros
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        tabla.addSubview(refresher)
        
        self.cerrarMapa.addTarget(self, action: "ocultarMapa", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.regionInicial = self.mapView.region
        println(self.regionInicial)
        self.mapView.setRegion(self.regionInicial, animated: false)
    }
    
    
    //[DLOPEZ]
    func refresh(){
        getRegistros()
    }
    
    override func viewDidAppear(animated: Bool) {
        showHUD(string: "Cargando...")
        getRegistros()
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.ocultarMapa()
    }
    
    func ocultarMapa(){
        UIView.animateWithDuration(0.5, animations:{
            self.mapView.frame = CGRect(x:self.xCentroMapa, y:self.yCentroMapa, width: 0, height: 0)
        })
        mapHidden = true
        fondoMapa.hidden = true
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.setRegion(self.regionInicial, animated: true)
        self.cerrarMapa.hidden = true
        self.tabla.userInteractionEnabled = true
//        self.mapView.hidden = true
    }
    
//    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        println("USANDOSE")
//        if(!mapHidden){
//            ocultarMapa()
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHUD(string: String? = nil){
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        if let text = string {
            hud.labelText = text
        }
    }
    
    func hideHUD() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:HistorialViewCell? = self.tabla.dequeueReusableCellWithIdentifier("registrosCell") as? HistorialViewCell
//        cell?.evento.text = self.items[indexPath.row]["tipo_evento"]
//        cell?.fecha.text = self.items[indexPath.row]["fch_servidor"]
        
        
        if self.items[indexPath.row]["tipo_evento"] != nil {
            if(self.items[indexPath.row]["tipo_evento"]=="Entrada"){
                cell?.evento.hidden = false
                cell?.eventoSalida.hidden = true
            }
            else{
                cell?.eventoSalida.hidden = false
                cell?.evento.hidden = true
            }
            
            cell?.mapa.image = UIImage(named: "mapa.png")
            if(self.items[indexPath.row]["val_latitud"] == nil || self.items[indexPath.row]["val_latitud"] == "0.000000"){
                cell?.mapa.image = UIImage(named: "mapaalpha.png")
            }
            
            if(self.items[indexPath.row]["dispositivo"] == "A" ){
                cell?.dispositivo.image = UIImage(named: "android.png")
            }
            else if(self.items[indexPath.row]["dispositivo"] == "I" ){
                cell?.dispositivo.image = UIImage(named: "apple.png")
            }
            else if(self.items[indexPath.row]["dispositivo"] == "P" ){
                cell?.dispositivo.image = UIImage(named: "tablet.png")
            }
            else{
                cell?.dispositivo.image = UIImage(named: "engage.png")
            }
            
            cell?.fecha.text = self.items[indexPath.row]["fch_servidor"]
            cell?.fecha.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
        }
        
        cell?.backgroundView = nil
        cell?.backgroundColor = UIColor.clearColor()
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        if(self.mapHidden==true && self.items[indexPath.row]["val_latitud"] != nil && self.items[indexPath.row]["val_latitud"] != "0.000000"){
            mapHidden = false
            
            var latitude: CLLocationDegrees = (items[indexPath.row]["val_latitud"]! as NSString).doubleValue
            var longitude: CLLocationDegrees = (items[indexPath.row]["val_longitud"]! as NSString).doubleValue
            var latDelta:CLLocationDegrees = 0.01
            var lonDelta:CLLocationDegrees = 0.01
            
            
            
            fondoMapa.hidden = false
//            self.cerrarMapa.hidden = false
            mapView.frame = CGRect(x:xCentroMapa, y:yCentroMapa, width: 0, height: 0)
            mapView.hidden = false
            self.tabla.userInteractionEnabled = false
            UIView.animateWithDuration(0.5, animations:{
                self.mapView.frame = CGRect(x:(self.xCentroMapa-self.centromapa), y:(self.yCentroMapa-self.centromapa), width: (self.ladomapa), height: (self.ladomapa))
                }, completion: {
                    (value: Bool) in
                    self.cerrarMapa.hidden = false
                    var span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
                    var location:CLLocationCoordinate2D =  CLLocationCoordinate2DMake(latitude, longitude)
                    var region = MKCoordinateRegion(center: location, span: span)
                    self.mapView.setRegion(region, animated: true)
                    
                    var annotation = MKPointAnnotation()
                    annotation.coordinate = location
                    self.mapView.addAnnotation(annotation)
                    
                    println(self.mapView.frame.width)
                    println(self.mapView.frame.height)
            })
        }
        else if(!mapHidden){
            println(indexPath.row)
            ocultarMapa()
        }

        
    }

    func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String!{
            return "Anular"
    }
    
    //PARA ANULAR UN REGISTRO CON SWIPE
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
        if(editingStyle == UITableViewCellEditingStyle.Delete){
            
            var beep_id: Int? = self.items[indexPath.row]["beep_id"]!.toInt()
            if(beep_id != nil){
                beepIn.annulRegister(beep_id!, {result in
                    if(result){
                        println("Registro anulado")
                    }
                    else{
                        println("Registro NO Anulado")
                    }
                })
                self.items.removeAtIndex(indexPath.row)
                self.tabla.reloadData()
            }
        }
    }
    
    func getRegistros() -> Void{
        beepIn.consultaRegistros({(results, success) in
            self.hideHUD()
            if success{
                self.items = results

                if (self.items.count == 0) {
                    var alert: UIAlertView = UIAlertView()
                    alert.title = "No hay registros"
                    alert.message = "Usuario no tiene registros en el sistema."
                    alert.addButtonWithTitle("Aceptar")
                    alert.show()
                }
                else {
                    
                    self.tabla.reloadData()
                }
            }
                
            else {
                var alert = UIAlertView()
                alert.title = "Error"
                alert.message = "No se pueden cargar los registros. Intente nuevamenta mas tarde."
                alert.addButtonWithTitle("Aceptar")
                alert.show()
            }
            self.refresher.endRefreshing()
        })
    }
    
}
