//
//  BeepIn.swift
//  ReportApp
//
//  Created by Carlos Orrego on 9/9/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import Foundation
import CoreLocation
//import Alamofire

class BeepIn : NSObject {
    private let service : BeepInService
    private let keychain = Keychain(service: "com.solu4b.beepIn", accessibility: .Always)
    var pkey = 0
    var semaforo = 1
    let settings = PersistedSettings.sharedInstance
    
    // singleton
    class var sharedInstance : BeepIn {
        struct Static {
            static let instance : BeepIn = BeepIn()
        }
        
        return Static.instance
    }
    
    override init() {
        // obtener credenciales (si existen)
        if let user = settings.user {
            if let account = keychain.accountFor(user) {
                // iniciar servicio con credenciales
                service = BeepInService(account: account)
                return
            }

            else {
                let (resultSet, err) = SD.executeQuery("SELECT * FROM latabla")
                if err != nil || resultSet.count == 0 {
                    //lala
                } else {
                    var row = resultSet.first!
                    var secretBD: String = row["lacolumna"]!.asString()!
                    var accountBD = Account(userName: user, secret: secretBD)
                    service = BeepInService(account: accountBD)
                    return
                }
            }
        }
        
        // iniciar servicio sin credenciales
        service = BeepInService()
    }
    
    func testConnectionAndCredentials(completed: (ConnectionResult -> Void)) {
        // todo: manejar modo offline
        service.testConnection { result in
            /*switch result{
            default:
                var a = "a"
                //println(result)
            }*/
            
            completed(result)
        }
    }
    
    func testConnectionWithCredentials(user: String!, password: String!, completed: (ConnectionResult -> Void)) {
        // crear credenciales
        //println("LLEGO USER:\(user) PASSWORD:\(password)")
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN BEEPIN.TESTWITHCREDENTIALS')")
        
        let credential = NSURLCredential(user: user, password: password, persistence: .None)
        let complete = completed;
        // probar
        service.testConnectionWithCredential(credential, {result in
            switch result {
            case .Success:
                //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN BEEPIN.SUCCESS')")
                // guardar credenciales
                /*println("usuario: \(user)")
                println("password: \(password)")
                let permCreds = NSURLCredential(user: user, password: password, persistence: .Permanent)
                println(permCreds.user)
                let credentialStore = NSURLCredentialStorage.sharedCredentialStorage()
                credentialStore.setDefaultCredential(permCreds, forProtectionSpace: self.service.protectionSpace)*/
                /*SD.executeChange("CREATE TABLE IF NOT EXISTS keychain (result TEXT)")
                SD.executeChange("DELETE FROM keychain")
                SD.executeChange("INSERT INTO keychain VALUES('\(password)')")*/
                var account = Account(userName: user, secret: password)
                // actualizar?
                if let oldacct = self.keychain.accountFor(user) {
                    self.keychain.update(account)
                }
                else {
                    self.keychain.add(account)
                }
                
                SD.executeChange("DELETE FROM latabla")
                SD.executeChange("INSERT INTO latabla VALUES('\(password!)')")
                
                self.settings.user = user
                //self.settings.password = password
                //Keychain.save(user, data: password)
                //SD.executeChange("INSERT INTO crashlog VALUES('GUARDE EN KEYCHAIN')")
            default:
                // no hacer nada
                break
            }
            
            // continuar
            complete(result)
        })
    }
    
    func consultaEstadoUsuario(completed: (AccionPermitida -> Void)) {
        service.estadoUsuario({result in
            switch result {
            case .Ingreso:
                completed(.Ingreso)
            case .Salida:
                completed(.Salida)
            default:
                break
            }
        })
    }
    
    func consultaRegistros(completed: (([[String:String]], Bool) -> Void)) {
        service.obtieneRegistros({(results, success) in
            completed(results, success)
        })
    }
    
    func reportArrivalOffline(params: [String : AnyObject]!, completed: (Bool -> Void)) {
        println("ENTRE A ARRIVAL ONLINE")
        service.testConnection({result in
            switch result{
            case .Success:
                self.service.reportArrival(params, {result in
                    switch result{
                    case .Success:
                        completed(true)
                    default:
                        completed(false)
                    }
                })
            default:
                completed(false)
            }
        })
    }
    
    func reportDepartureOffline(params: [String : AnyObject]!, completed: (Bool -> Void)) {
        println("ENTRE A DEPARTURE OFFLINE")
        service.testConnection({result in
            switch result{
            case .Success:
                self.service.reportDeparture(params, {result in
                    switch result{
                    case .Success:
                        completed(true)
                    default:
                        completed(false)
                    }
                })
            default:
                completed(false)
            }
        })
    }
    
    func annulRegister(beepId: Int,  completed: (Bool -> Void)){
        var params:[String:Int] = ["beep_id": beepId]
        
        service.testConnection({result in
            switch result{
            case .Success:
                println("TENGO INTERNET Y LO ANULO")
                self.service.annulRegister(params, {result in
                    switch result{
                    case .Success:
                        completed(true)
                    default:
                        completed(false)
                    }
                })
            default:
                println("NO TENGO INTERNET SE DESCARTA LA ANULACIÓN")
                completed(false)
            }
        })
    }
    
    func reportArrival(location: CLLocation?, completed: (Bool -> Void)){
        var todaysDate:NSDate = NSDate()
        var dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
        
        var toArray = DateInFormat.componentsSeparatedByString(" ")
        var fecha_off = join("T", toArray)
        var fecha = "datetime'"+join("T", toArray)+"'"
        
        var params:[String:AnyObject] = ["fch_dispositivo": fecha, "nro_retraso_sec":0, "val_latitud":"0M", "val_longitud":"0M", "val_altura":"0", "val_precision":"0", "tipo_evento":"'I'", "tipo_dispositivo":"'I'", "es_mock_location": "true", "es_registro_offline": "false", "beacon_primario_major": "", "beacon_primario_minor": "", "beacon_secundario_major": "", "beacon_secundario_minor": 0]
        
        if let loc = location{
            params["es_mock_location"] = "false"
            params["val_latitud"] = "\(loc.coordinate.latitude)M"
            params["val_longitud"] = "\(loc.coordinate.longitude)M"
            params["val_altura"] = loc.altitude
            params["val_precision"] = loc.horizontalAccuracy
        }
        
        let fec: AnyObject = params["fch_dispositivo"]!
        let nro: AnyObject = params["nro_retraso_sec"]!
        let lat: AnyObject = params["val_latitud"]!
        let lon: AnyObject = params["val_longitud"]!
        let alt: AnyObject = params["val_altura"]!
        let pre: AnyObject = params["val_precision"]!
        let eve: AnyObject = params["tipo_evento"]!
        let dis: AnyObject = params["tipo_dispositivo"]!
        let moc: AnyObject = params["es_mock_location"]!
        let bpa: AnyObject = params["beacon_primario_major"]!
        let bpb: AnyObject = params["beacon_primario_minor"]!
        let bsa: AnyObject = params["beacon_secundario_major"]!
        let bsb: AnyObject = params["beacon_secundario_minor"]!
        
        let query = "INSERT INTO offline (fch_dispositivo, nro_retraso_sec, val_latitud, val_longitud, val_altura, val_precision, tipo_evento, tipo_dispositivo, es_mock_location, es_registro_offline, beacon_primario_major, beacon_primario_minor, beacon_secundario_major, beacon_secundario_minor, user) VALUES ('\(fecha_off)', '\(nro)', '\(lat)', '\(lon)', '\(alt)', '\(pre)', \(eve), \(dis), '\(moc)', 'true', '\(bpa)', '\(bpb)', '\(bsa)', '\(bsb)', '\(self.settings.user!)')"
        
        service.testConnection({result in
            switch result{
            case .Success:
                println("TENGO INTERNET Y LO ENVIO")
                self.service.reportArrival(params, {result in
                    switch result{
                    case .Success:
                        completed(true)
                    default:
                        completed(false)
                    }
                })
            default:
                println("NO TENGO INTERNET Y LO GUARDO")
                SD.executeChange(query)
                completed(true)
            }
        })
    }
    
    func reportDeparture(location: CLLocation?, completed: (Bool -> Void)){
        var todaysDate:NSDate = NSDate()
        var dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
        
        var toArray = DateInFormat.componentsSeparatedByString(" ")
        var fecha_off = join("T", toArray)
        var fecha = "datetime'"+join("T", toArray)+"'"
        
        var params:[String:AnyObject] = ["fch_dispositivo": fecha, "nro_retraso_sec":0, "val_latitud":"0M", "val_longitud":"0M", "val_altura":"0", "val_precision":"0", "tipo_evento":"'S'", "tipo_dispositivo":"'I'", "es_mock_location": "true", "es_registro_offline": "false", "beacon_primario_major": "", "beacon_primario_minor": "", "beacon_secundario_major": "", "beacon_secundario_minor": 0]
        
        if let loc = location{
            params["es_mock_location"] = "false"
            params["val_latitud"] = "\(loc.coordinate.latitude)M"
            params["val_longitud"] = "\(loc.coordinate.longitude)M"
            params["val_altura"] = loc.altitude
            params["val_precision"] = loc.horizontalAccuracy
        }
        
        let fec: AnyObject = params["fch_dispositivo"]!
        let nro: AnyObject = params["nro_retraso_sec"]!
        let lat: AnyObject = params["val_latitud"]!
        let lon: AnyObject = params["val_longitud"]!
        let alt: AnyObject = params["val_altura"]!
        let pre: AnyObject = params["val_precision"]!
        let eve: AnyObject = params["tipo_evento"]!
        let dis: AnyObject = params["tipo_dispositivo"]!
        let moc: AnyObject = params["es_mock_location"]!
        let bpa: AnyObject = params["beacon_primario_major"]!
        let bpb: AnyObject = params["beacon_primario_minor"]!
        let bsa: AnyObject = params["beacon_secundario_major"]!
        let bsb: AnyObject = params["beacon_secundario_minor"]!
        
        let query = "INSERT INTO offline (fch_dispositivo, nro_retraso_sec, val_latitud, val_longitud, val_altura, val_precision, tipo_evento, tipo_dispositivo, es_mock_location, es_registro_offline, beacon_primario_major, beacon_primario_minor, beacon_secundario_major, beacon_secundario_minor, user) VALUES ('\(fecha_off)', '\(nro)', '\(lat)', '\(lon)', '\(alt)', '\(pre)', \(eve), \(dis), '\(moc)', 'true', '\(bpa)', '\(bpb)', '\(bsa)', '\(bsb)', '\(self.settings.user!)')"
        service.testConnection({result in
            switch result{
            case .Success:
                println("TENGO INTERNET Y LO ENVIO")
                self.service.reportDeparture(params, {result in
                    switch result{
                    case .Success:
                        completed(true)
                    default:
                        completed(false)
                    }
                })
            default:
                println("NO TENGO INTERNET Y LO GUARDO")
                SD.executeChange(query)
                completed(true)
            }
        })
    }
    
    func enviarOffline() {
        println("ENVIAROFFLINE")
        //var array : Array = []
        //var semaforo = 1 // 0 rojo, 1 verde
        //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
        var params:[String:AnyObject] = ["fch_dispositivo": "", "nro_retraso_sec":0, "val_latitud":"0M", "val_longitud":"0M", "val_altura":"0", "val_precision":"0", "tipo_evento":"", "tipo_dispositivo":"", "es_mock_location": "true", "es_registro_offline": "true", "beacon_primario_major": "", "beacon_primario_minor": "", "beacon_secundario_major": "", "beacon_secundario_minor": 0]
        
        //var pkey = 0
        var eve = ""
        var dis = ""
        
        let (resultSet, err) = SD.executeQuery("SELECT * FROM offline order by pkey asc")
        if err != nil {
            //there was an error during the query, handle it here
        } else {
            
            if resultSet.count != 0 {
//                var todaysDate:NSDate = NSDate()
//                var dateFormatter:NSDateFormatter = NSDateFormatter()
//                dateFormatter.dateFormat = "dd-MM-yyyy '-' HH:mm"
//                var DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
//                
//                let query = "INSERT INTO ultimo_envio_offline (fch_envio) VALUES('\(DateInFormat)')"
//                SD.executeChange(query)
                self.settings.lastOffline = NSDate()
            }
            
            for row in resultSet {
                while self.semaforo == 0 {
                    //println("EL SEMAFORO ESTA EN ROJO")
                }

                var usr = row["user"]!.asString()!
                
                self.pkey = row["pkey"]!.asInt()!
                
                eve = row["tipo_evento"]!.asString()!
                dis = row["tipo_dispositivo"]!.asString()!
            
                params["fch_dispositivo"] = "datetime'"+row["fch_dispositivo"]!.asString()!+"'"
                params["nro_retraso_sec"] = row["nro_retraso_sec"]!.asString()
                params["val_latitud"] = row["val_latitud"]!.asString()
                params["val_longitud"] = row["val_longitud"]!.asString()
                params["val_altura"] = row["val_altura"]!.asString()
                params["val_precision"] = row["val_precision"]!.asString()
                //params["tipo_evento"] = row["tipo_evento"]!.asString()
                params["tipo_evento"] = "'\(eve)'"
                //params["tipo_dispositivo"] = row["tipo_dispositivo"]!.asString()
                params["tipo_dispositivo"] = "'\(dis)'"
                params["es_mock_location"] = row["es_mock_location"]!.asString()
                params["beacon_primario_major"] = row["beacon_primario_major"]!.asString()
                params["beacon_primario_minor"] = row["beacon_primario_minor"]!.asString()
                params["beacon_secundario_major"] = row["beacon_secundario_major"]!.asString()
                //params["beacon_secundario_minor"] = row["beacon_secundario_minor"]!.asString()
                params["beacon_secundario_minor"] = pkey

                self.semaforo = 0
                
                //println("TENGO \(params)")
                
                println("AHORA VIENE \(self.pkey)")
                if usr != settings.user! {
                    
                    var accountBD: Account!

                    if var ac = keychain.accountFor(usr) {
                        accountBD = ac
                    }
                    else {
                        let (resultSet, err) = SD.executeQuery("SELECT * FROM latabla")
                        if err != nil {
                            //lala
                        } else {
                            var row = resultSet.first!
                            var secretBD: String = row["lacolumna"]!.asString()!
                            accountBD = Account(userName: usr, secret: secretBD)
                        }
                    }
                    
                    var service_offline = BeepInService(account: accountBD)
                    
                    service_offline.testConnection({result in
                        println("PROBANDO CONEXION")
                        switch result{
                        case .Success:
                            println("TENGO INTERNET Y LO ENVIO")
                            println("VOY A ELIMINAR A: \(self.pkey)")
                            //SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
                            service_offline.reportArrival(params, {result in
                                switch result{
                                case .Success:
                                    SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
                                    self.semaforo = 1
                                default:
                                    self.semaforo = 1
                                }
                            })
                        default:
                            self.semaforo = 1
                        }
                    })
                }
                else {
                    service.testConnection({result in
                        println("PROBANDO CONEXION")
                        switch result{
                            case .Success:
                                println("TENGO INTERNET Y LO ENVIO")
                                println("VOY A ELIMINAR A: \(self.pkey)")
                                //SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
                                self.service.reportArrival(params, {result in
                                    switch result{
                                    case .Success:
                                        SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
                                        self.semaforo = 1
                                    default:
                                        self.semaforo = 1
                                    }
                                })
                            default:
                                self.semaforo = 1
                        }
                    })
                }
            }
        }
    }
    
    func enviarUnoOffline(completed: ConnectionResult  -> Void){
        println("ENVIARUNOOFFLINE")
//        var todaysDate:NSDate = NSDate()
//        var dateFormatter:NSDateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy '-' HH:mm"
//        var DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
//        
//        let query = "INSERT INTO ultimo_envio_offline (fch_envio) VALUES('\(DateInFormat)')"
//        SD.executeChange(query)
        
        var params:[String:AnyObject] = ["fch_dispositivo": "", "nro_retraso_sec":0, "val_latitud":"0M", "val_longitud":"0M", "val_altura":"0", "val_precision":"0", "tipo_evento":"", "tipo_dispositivo":"", "es_mock_location": "true", "es_registro_offline": "true", "beacon_primario_major": "", "beacon_primario_minor": "", "beacon_secundario_major": "", "beacon_secundario_minor": 0]
        
        let (resultSet, err) = SD.executeQuery("SELECT * FROM offline order by pkey asc")
        if err != nil || resultSet.count == 0 {
            //there was an error during the query, handle it here
            completed(.NoConnection)
        } else {
            
//            var todaysDate:NSDate = NSDate()
//            var dateFormatter:NSDateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "dd-MM-yyyy '-' HH:mm"
//            var DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
//            
//            let query = "INSERT INTO ultimo_envio_offline (fch_envio) VALUES('\(DateInFormat)')"
//            SD.executeChange(query)
            self.settings.lastOffline = NSDate()
            
            var row = resultSet.first!
            
            var usr = row["user"]!.asString()!
            
            self.pkey = row["pkey"]!.asInt()!
            var eve = ""
            var dis = ""
            eve = row["tipo_evento"]!.asString()!
            dis = row["tipo_dispositivo"]!.asString()!
            
            params["fch_dispositivo"] = "datetime'"+row["fch_dispositivo"]!.asString()!+"'"
            params["nro_retraso_sec"] = row["nro_retraso_sec"]!.asString()
            params["val_latitud"] = row["val_latitud"]!.asString()
            params["val_longitud"] = row["val_longitud"]!.asString()
            params["val_altura"] = row["val_altura"]!.asString()
            params["val_precision"] = row["val_precision"]!.asString()
            //params["tipo_evento"] = row["tipo_evento"]!.asString()
            params["tipo_evento"] = "'\(eve)'"
            //params["tipo_dispositivo"] = row["tipo_dispositivo"]!.asString()
            params["tipo_dispositivo"] = "'\(dis)'"
            params["es_mock_location"] = row["es_mock_location"]!.asString()
            params["beacon_primario_major"] = row["beacon_primario_major"]!.asString()
            params["beacon_primario_minor"] = row["beacon_primario_minor"]!.asString()
            params["beacon_secundario_major"] = row["beacon_secundario_major"]!.asString()
            //params["beacon_secundario_minor"] = row["beacon_secundario_minor"]!.asString()
            params["beacon_secundario_minor"] = pkey
            
            //self.semaforo = 0
            
            //println("TENGO \(params)")
            
            println("AHORA VIENE \(self.pkey)")
//            service.testConnection({result in
//                println("PROBANDO CONEXION")
//                switch result{
//                case .Success:
//                    println("TENGO INTERNET Y LO ENVIO")
//                    
//                    self.service.reportArrival(params, {result in
//                        switch result{
//                        case .Success:
//                            println("VOY A ELIMINAR A: \(self.pkey)")
//                            SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
//                            completed(.Success)
//                        default:
//                            completed(result)
//                        }
//                    })
//                default:
//                    completed(result)
//                }
//            })
            if usr != settings.user! {
                
                var accountBD: Account!
                
                if var ac = keychain.accountFor(usr) {
                    accountBD = ac
                }
                else {
                    let (resultSet, err) = SD.executeQuery("SELECT * FROM latabla")
                    if err != nil {
                        //lala
                    } else {
                        var row = resultSet.first!
                        var secretBD: String = row["lacolumna"]!.asString()!
                        accountBD = Account(userName: usr, secret: secretBD)
                    }
                }
                
                var service_offline = BeepInService(account: accountBD)
                
                service_offline.testConnection({result in
                    switch result{
                        case .Success:
                            service_offline.reportArrival(params, {result in
                                switch result{
                                case .Success:
                                    SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
                                    completed(.Success)
                                default:
                                    completed(result)
                                }
                            })
                        default:
                            completed(result)
                    }
                })
            }
            else {
                service.testConnection({result in
                    switch result{
                    case .Success:
                        
                        self.service.reportArrival(params, {result in
                            switch result{
                            case .Success:
                                SD.executeChange("DELETE FROM offline WHERE pkey = \(self.pkey)")
                                completed(.Success)
                            default:
                                completed(result)
                            }
                        })
                    default:
                        completed(result)
                    }
                })
            }
        }
    }
    
    func logOut() {
        /*let credentialStore = NSURLCredentialStorage.sharedCredentialStorage()
        // obtener credenciales para el espacio
        if let creds = credentialStore.credentialsForProtectionSpace(service.protectionSpace)?.values.array {
            for cred in creds {
                if let cred2 = cred as? NSURLCredential {
                    credentialStore.removeCredential(cred2, forProtectionSpace: service.protectionSpace)
                }
            }
        }*/
        // eliminar credenciales
        
        // [DLOPEZ]
        // se limpia la tabla de datos offline al cerrar la sesión
        SD.executeChange("DELETE FROM offline")
        // eliminar usuario
        settings.user = nil
        service.endSession()
        //Alamofire.Manager.sharedInstance.session.finalize()
    }
}