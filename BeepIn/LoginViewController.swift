//
//  vistaLoginViewController.swift
//  BeepIn
//
//  Created by Carlos Orrego on 11/5/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var ingresarbtn: UIButton!
    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var contrasena: UITextField!
    
    var persistedSettings = PersistedSettings.sharedInstance
    var beepIn = BeepIn.sharedInstance
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        let subViews: Array = self.view.subviews
        for sub in subViews
        {
            if sub.tag == 1234 {
                sub.removeFromSuperview()
                var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBarHidden = true
        
        var backgroundView = BackgroundUIImageView(view: self.view, image: UIImage(named: "bubbles2.png")!)
        
        // imagenes de textbox
        usuario.leftViewMode = .Always
        usuario.leftView = UIImageView(image: UIImage(named: "user.png")!)
        usuario.leftView?.contentMode = .ScaleAspectFit
        usuario.leftView?.frame = CGRect(x: 0,y: 0,width: usuario.frame.height*0.9, height: usuario.frame.height*0.9)
        
        contrasena.leftViewMode = .Always
        contrasena.leftView = UIImageView(image: UIImage(named: "llave.png")!)
        contrasena.leftView?.contentMode = .ScaleAspectFit
        contrasena.leftView?.frame = CGRect(x: 0,y: 0,width: contrasena.frame.height*0.9, height: contrasena.frame.height*0.9)
        
        usuario.delegate = self
        contrasena.delegate = self
        
        // estamos logueados?
        showHUD(string: "Verificando...")
        beepIn.testConnectionAndCredentials({ result in
            switch result{
                case .Success:
                    self.hideHUD()
                    
                    self.performSegueWithIdentifier("registroSegue", sender: self)
                    
    //                if self.persistedSettings.user == "mmella" {
    //                    let tabbar = self.storyboard?.instantiateViewControllerWithIdentifier("tabbarView") as? UITabBarController
    //                    println(tabbar?.viewControllers?.count)
    //                    self.presentViewController(tabbar!, animated: true, completion: nil)
//                        self.navigationController?.pushViewController(tabbar!, animated: true)
    //                }
    //                else {
    //                    self.performSegueWithIdentifier("pruebaSegue", sender: self) //se cae en testflight
    //                }

                default:
                    if self.persistedSettings.user != nil {
                        // esto es por si la persona estaba logueada y, al momento de abrir la aplicacion, no tiene internet (creo que es un "modo" útil).
                        self.performSegueWithIdentifier("registroSegue", sender: self)
                    }
                    self.hideHUD()
                    break
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
//        showHUD(string: "Verificando...")
//        beepIn.testConnectionAndCredentials({ result in
//            switch result{
//            case .Success:
//                self.hideHUD()
//                self.performSegueWithIdentifier("registroSegue", sender: self)
//            default:
//                self.hideHUD()
//                break
//            }
//        })
    }
        
    func showHUD(string: String? = nil){
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        if let text = string {
            hud.labelText = text
        }
    }
    
    func hideHUD() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    override func viewDidDisappear(animated: Bool) {
        //SD.executeChange("INSERT INTO crashlog VALUES('VOY A SALIR DE LOGIN')")
    }
    
    @IBAction func login(sender: AnyObject) {
        self.view.endEditing(true)
        
        showHUD(string: "Ingresando...")
        
        let user = usuario.text!
        let password = contrasena.text!
        
        //SD.executeChange("INSERT INTO crashlog VALUES('ESTOY EN LOGIN.TESTCONNECTIONWITHCREDENTIALS')")
        
        beepIn.testConnectionWithCredentials(user, password: password, {result in
            self.hideHUD()
            
            switch result {
                case .NoCredentials:
                    var alert = UIAlertView()
                    alert.title = "No se puede ingresar"
                    alert.message = "Nombre de usuario o contraseña incorrectos."
                    alert.addButtonWithTitle("Aceptar")

                    alert.show()
                case .Success:
                    
                    self.persistedSettings.user = user
                    self.performSegueWithIdentifier("registroSegue", sender: self)

//                    if self.persistedSettings.user == "mmella" {
//                        let tabbar = self.storyboard?.instantiateViewControllerWithIdentifier("tabbarView") as? UITabBarController
//                        println(tabbar?.viewControllers?.count)
//                        self.presentViewController(tabbar!, animated: true, completion: nil)
//                    }
//                    else {
//                        self.performSegueWithIdentifier("pruebaSegue", sender: self) //se cae en testflight
//                    }
                    
                    //SD.executeChange("INSERT INTO crashlog VALUES('TERMINE EN LOGIN.SUCCESS')")
                    
                default:
                    var alert = UIAlertView()
                    alert.title = "No se puede ingresar"
                    alert.message = "Ha fallado la comunicación con el servidor. Verifique su conexión a internet."
                    alert.addButtonWithTitle("Aceptar")
                    alert.show()
            }
        })
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
}
