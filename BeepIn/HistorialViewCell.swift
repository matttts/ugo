//
//  registroCelda.swift
//  BeepIn
//
//  Created by Matías Mella on 11/12/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import UIKit

class HistorialViewCell: UITableViewCell {
    
    @IBOutlet weak var fecha: UILabel!
    @IBOutlet weak var evento: UIImageView!
    @IBOutlet weak var eventoSalida: UIImageView!
    @IBOutlet weak var mapa: UIImageView!
    @IBOutlet weak var dispositivo: UIImageView!

    @IBOutlet weak var fondoEvento: UILabel!
    @IBOutlet weak var fondoMapaDisp: UILabel!
    @IBOutlet weak var fondoFecha: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
}
