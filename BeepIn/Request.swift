//
//  Request.swift
//  BeepIn
//
//  Created by Carlos Orrego on 10/16/14.
//  Copyright (c) 2014 Carlos Orrego. All rights reserved.
//

import Foundation

class Request : NSObject {
    func send(url: String, f: (String)-> ()) {
        var request = NSURLRequest(URL: NSURL(string: url))
        var response: NSURLResponse?
        var error: NSErrorPointer = nil
        var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: error)
        var reply = NSString(data: data!, encoding: NSUTF8StringEncoding)
        f(reply)
    }
}